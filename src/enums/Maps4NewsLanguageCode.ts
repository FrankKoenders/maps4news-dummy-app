// Values from: https://docs.maps4news.com/v1/index.html?javascript--wrapper#building-a-map
export enum Maps4NewsLanguageCode {
	English = "eng",
	Dutch = "dut",
	Italian = "ita",
	German = "ger"
}
