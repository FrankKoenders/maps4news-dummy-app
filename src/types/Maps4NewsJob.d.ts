import { Maps4NewsCrudBase } from "@/types/Maps4NewsCrudBase";
import { Maps4NewsJobRevision } from "@/types/Maps4NewsJobRevision";
import { Maps4NewsJobData } from "@/types/Maps4NewsJobData";
import { Maps4NewsJobRevisionData } from "@/types/Maps4NewsJobRevisionData";

export interface Maps4NewsJob extends Maps4NewsCrudBase<Maps4NewsJob>, Maps4NewsJobData {
	revisions: {
		new: (data: Maps4NewsJobRevisionData) => Maps4NewsJobRevision;
	};
}
