export interface ApiSettings {
	id: number;
	host: string;
	callback: string;
}
