import { ApiSettings } from "@/types/ApiSettings";

export interface Store {
	settings: {
		api: ApiSettings;
	};
	userInfo: {
		name: string | null;
		email: string | null;
	};
}
