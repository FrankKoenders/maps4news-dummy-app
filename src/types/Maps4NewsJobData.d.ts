import { Maps4NewsJobType } from "@/enums/Maps4NewsJobType";

export interface Maps4NewsJobData {
	jobTypeId: Maps4NewsJobType;
	title: string;
}
