export interface UserInfoSetPayload {
	name: string;
	email: string;
}
