import { Maps4NewsUserInfo } from "@/types/Maps4NewsUserInfo";
import { Maps4NewsJob } from "@/types/Maps4NewsJob";
import { Maps4NewsJobData } from "@/types/Maps4NewsJobData";
import { Maps4NewsMapStylesSetsResult } from "@/types/Maps4NewsMapStylesSetsResult";

// Minimal typings files for maps4news API
export interface Maps4NewsApi {
	// Methods
	authenticate: () => Promise<void>;
	saveToken: () => void;

	// Users
	users: {
		get (id: string | number | object): Promise<Maps4NewsUserInfo>;
		select (id: string | number | object): {
			mapstyleSets: {
				list: () => Promise<Maps4NewsMapStylesSetsResult>;
			};
		};
	};

	// Jobs
	jobs: {
		new: (data: Maps4NewsJobData) => Maps4NewsJob;
	};

	// Properties
	authenticated: boolean;
}
