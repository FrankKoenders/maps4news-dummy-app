import { Maps4NewsMapStylesSet } from "@/types/Maps4NewsMapStylesSet";

export interface Maps4NewsMapStylesSetsResult {
	data: Array<Maps4NewsMapStylesSet>;
}
