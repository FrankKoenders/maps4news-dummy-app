// Based on api-wrapper/src/resources/base/CrudBase.js.
export interface Maps4NewsCrudBase<T> {
	// Save item. This will create a new item if `id` is unset.
	save: (...args: unknown[]) => Promise<T>;

	// Restore item
	restore: (updateSelf: boolean) => Promise<T>;

	// Delete
	delete: (updateSelf: boolean) => Promise<T>;
}
