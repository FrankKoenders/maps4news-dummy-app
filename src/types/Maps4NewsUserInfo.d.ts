// Types constructed from observations. Nullability sometimes guessed.
export interface Maps4NewsUserInfo {
	address: string | null;
	city: string | null;
	company: string | null;
	confirmed: boolean;
	country: string | null;
	createdAt: Date;
	deleted: boolean;
	deletedAt: Date | null;
	email: string | null;
	id: number;
	languageCode: string | null;
	name: string | null;
	notificationsCheckedAt: Date | null;
	organisationId: number;
	phone: string | null;
	profession: string | null;
	referral: string; /* JSON */
	tags: null; /* array? */
	tips: boolean;
	updatedAt: Date;
}
