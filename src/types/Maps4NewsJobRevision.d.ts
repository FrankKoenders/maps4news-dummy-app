import { Maps4NewsJobRevisionData } from "@/types/Maps4NewsJobRevisionData";
import { Maps4NewsCrudBase } from "@/types/Maps4NewsCrudBase";
import { Maps4NewsBuild } from "@/types/Maps4NewsBuild";
import { Maps4NewsResult } from "@/types/Maps4NewsResult";

export interface Maps4NewsJobRevision extends Maps4NewsCrudBase<Maps4NewsJobRevision>, Maps4NewsJobRevisionData {
	// TODO: define types instead of unknown
	save: (object: unknown, layers: unknown) => Promise<Maps4NewsJobRevision>;
	// callbackUrl - Optional callback url for when the job completes
	build: (callbackUrl?: string) => Promise<Maps4NewsBuild>;
	result: () => Promise<Maps4NewsResult>;
}
