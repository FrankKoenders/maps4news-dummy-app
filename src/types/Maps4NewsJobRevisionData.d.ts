import { Maps4NewsLanguageCode } from "@/enums/Maps4NewsLanguageCode";
import { Maps4NewsMapStyle } from "@/enums/Maps4NewsMapStyle";

export interface Maps4NewsJobRevisionData {
	languageCode: Maps4NewsLanguageCode;
	mapstyleSetId: Maps4NewsMapStyle;
}
