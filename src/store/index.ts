import Vue from "vue";
import Vuex, { ActionContext } from "vuex";
import { Store } from "@/types/Store";
import * as Actions from "@/store/Actions";
import * as Mutations from "@/store/Mutations";

// Interfaces
import { Maps4NewsUserInfo } from "@/types/Maps4NewsUserInfo";
import { UserInfoSetPayload } from "@/types/UserInfoSetPayload";

Vue.use(Vuex);

export default new Vuex.Store<Store>({
	state: {
		settings: {
			api: {
				id: 75,
				host: "https://api.maps4news.com",
				callback: "http://localhost:8080/"
			}
		},
		userInfo: {
			name: null,
			email: null
		}
	},
	mutations: {
		[Mutations.USER_INFO_SET] (state: Store, payload: UserInfoSetPayload) {
			state.userInfo.name = payload.name;
			state.userInfo.email = payload.email;
		}
	},
	actions: {
		[Actions.USER_LOGIN] (context: ActionContext<Store, Store>, userInfo: Maps4NewsUserInfo) {
			console.log("userInfo", userInfo);
			context.commit(Mutations.USER_INFO_SET, {
				name: userInfo.name,
				email: userInfo.email
			} as UserInfoSetPayload);
		}
	},
	modules: {
	},
	getters: {
		hasUserInfo (state: Store) {
			return state.userInfo.name != null && state.userInfo.email != null;
		}
	}
});
